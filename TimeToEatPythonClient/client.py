#!/usr/bin/python3
# Time to Eat Python Client v1.0
# To use this, you must install pyqt5, toml, and playsound.

import toml
from PyQt5.QtWidgets import QApplication, QMessageBox
from PyQt5 import QtCore
import threading
import requests
import queue
import os
import sys
from sys import platform
from time import sleep
from playsound import playsound


# Load configuration
with open('config.toml') as f:
	config = toml.loads(f.read())


# Get server address and message
address = 'http://' + config['server']['ip_address'] + ':' + str(config['server']['port']) + '/timetoeat/' + config['client']['id']
message = config['message']


# Create message box
app = QApplication(sys.argv)
msgBox = QMessageBox()
msgBox.setIcon(QMessageBox.Information)
msgBox.setText(message)
msgBox.setWindowTitle(message)
msgBox.setStandardButtons(QMessageBox.Ok)
msgBox.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)


# Create a Queue, which allows us to run code on the main thread.
# This is necessary because Qt code can only be run on the main thread.
queue = queue.Queue()


# Start the main loop
def loop():
	while True:
		try:
			timetoeat = requests.get(address).text
			if timetoeat == 'true':
				# Play an alert sound
				playsound('alert.wav', False)

				# Show a notification on Linux
				if platform == "linux" or platform == "linux2":
					os.system('notify-send "' + message + '" "' + message + '"')

				# Show the message box
				queue.put(msgBox.exec)
		except:
			print("Failed to connect to the server.")
		sleep(1)
threading.Thread(target=loop).start()

# Show the message box when necessary
while True:
	callback = queue.get()
	callback()
