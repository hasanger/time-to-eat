package io.sanger.henry.timetoeat;

import com.moandjiezana.toml.Toml;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TimeToEatClient {

    public static String IP_ADDRESS, ID;
    public static int PORT;

    public static String MESSAGE;

    public static void main(String[] args) {

        // Load configuration
        Toml config = new Toml().read(new File("config.toml"));

        MESSAGE = config.getString("message");

        // Server section
        Toml server = config.getTable("server");
        IP_ADDRESS = server.getString("ip_address");
        PORT = server.getLong("port").intValue();

        // Client section
        ID = config.getTable("client").getString("id");


        // Create invisible host JFrame, which allows us to create always-on-top message boxes
        JFrame hostFrame = new JFrame();
        hostFrame.setAlwaysOnTop(true);

        while(true) {
            try {
                // Connect to the server
                StringBuilder result = new StringBuilder();
                URL url = new URL("http://" + IP_ADDRESS + ":" + PORT + "/timetoeat/" + ID);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                    result.append("\n");
                }
                rd.close();

                // Show a message box and play a sound if the server returns true.
                // On Linux, show a notification.
                if(result.toString().trim().equals("true")) {
                    Clip clip = AudioSystem.getClip();
                    clip.open(AudioSystem.getAudioInputStream(new File("alert.wav")));
                    clip.start();
                    if(System.getProperty("os.name").equals("Linux")) new ProcessBuilder("notify-send", MESSAGE, MESSAGE).start();
                    new Thread(() -> JOptionPane.showMessageDialog(hostFrame, MESSAGE)).start();
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch(InterruptedException ignored) {}
        }
    }

}

