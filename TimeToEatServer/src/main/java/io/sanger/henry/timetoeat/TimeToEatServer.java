package io.sanger.henry.timetoeat;

import com.moandjiezana.toml.Toml;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import static spark.Spark.*;

public class TimeToEatServer {

    public static String MESSAGE;

    public static String IP_ADDRESS;
    public static int PORT;

    public static String SERVER, SENDER, PASSWORD;
    public static List<String> EMAIL_RECIPIENTS;

    public static final HashMap<String, Boolean> TIME_TO_EAT_MAP = new HashMap<>();

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("EEE MMM dd, yyyy hh:mm:ss a");

    public static void main(String[] args) {

        // Parse config.toml

        Toml config = new Toml().read(new File("config.toml"));

        MESSAGE = config.getString("message");

        // Server section
        Toml server = config.getTable("server");

        IP_ADDRESS = server.getString("ip_address");
        PORT = server.getLong("port").intValue();

        // Email section
        Toml email = config.getTable("email");
        boolean emailEnabled = !(email.contains("disabled") && email.getBoolean("disabled"));

        if(emailEnabled) {
            SENDER = email.getString("sender");
            SERVER = email.getString("server");
            PASSWORD = email.getString("password");
        }

        // Recipients section
        Toml recipients = config.getTable("recipients");
        for(String id : recipients.<String>getList("ids")) TIME_TO_EAT_MAP.put(id, false);
        if(emailEnabled) {
            EMAIL_RECIPIENTS = recipients.getList("addresses");
            if(EMAIL_RECIPIENTS == null) EMAIL_RECIPIENTS = Collections.emptyList();
        }


        // Set up server
        ipAddress(IP_ADDRESS);
        port(PORT);
        get("/timetoeat/:id", (req, res) -> {
            String id = req.params("id");
            if(id == null || !TIME_TO_EAT_MAP.containsKey(id)) return false;
            boolean timeToEat = TIME_TO_EAT_MAP.get(id);
            if(timeToEat) TIME_TO_EAT_MAP.put(id, false);
            return timeToEat;
        });

        // Create JFrame. If the space key is pressed in this window,
        // the program will send the message specified in config.toml to the recipients.
        JFrame frame = new JFrame("Time To Eat v4.0");
        frame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if(e.getKeyChar() == ' ') {
                    System.out.println("[" + FORMATTER.format(LocalDateTime.now()) + "] " + MESSAGE);
                    TIME_TO_EAT_MAP.replaceAll((i, v) -> true);

                    // Send email
                    if(!email.getBoolean("disabled")) {
                        try {
                            Properties props = System.getProperties();
                            props.setProperty("mail.smtp.host", SERVER);
                            props.setProperty("mail.user", SENDER);
                            props.setProperty("mail.password", PASSWORD);

                            Session session = Session.getDefaultInstance(props);

                            for(String recipient : EMAIL_RECIPIENTS) {
                                MimeMessage message = new MimeMessage(session);
                                message.setFrom(new InternetAddress(SENDER));
                                message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
                                message.setSubject(MESSAGE);
                                message.setText(MESSAGE);

                                Transport.send(message);
                            }
                        } catch(Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
            @Override public void keyPressed(KeyEvent e) {}
            @Override public void keyReleased(KeyEvent e) {}
        });

        // Configure and show the JFrame
        frame.setSize(800, 800);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
