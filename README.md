# Time To Eat

A simple project that allows you to send a predefined message to a number of computers and/or email addresses.

## Setup

1. [Download the repo](https://gitlab.com/hasanger/time-to-eat/-/archive/main/time-to-eat-main.zip) and extract.

2. Open `config.toml` in your favorite text editor.

3. Replace `Time to eat!` with the message you want to send.

4. Replace `192.168.0.0` with the local IP address of the computer you want to use as the server. [This article](https://lifehacker.com/how-to-find-your-local-and-external-ip-address-5833108) explains how to find the local IP address.

5. If you want to use the program to send emails, replace `mail.example.com` with the URL of your email server.

   Replace `sender@example.com` with your email address, and `password` with your password.
   
   If you're using Gmail, your email server is `smtp.gmail.com`. You may need to [turn on less secure app access](https://myaccount.google.com/lesssecureapps).

   If you don't want to send emails, replace `disabled = false` with `disabled = true`.

6. Come up with an ID for each computer you want to send the message to: something like `downstairs` or `office`.

7. On the `ids` line, replace `example` and `example2` with the IDs of the computers. If you don't plan on using the client program, you can leave the array blank (`ids = []`). If you get an error on startup, try escaping special characters like backslashes and double quotes; [more on that here](https://toml.io/en/v1.0.0#string).

8. If you're using the email feature: On the `addresses` line, replace `recipient@example.com` and `recipient2@example.com` with the email addresses of the people you want to send the message to. You can add or remove items from the array.


### Setting up the server
9. Copy `config.toml` and `TimeToEatServer-4.0-all.jar` (in the Compiled folder) to the server computer. Make sure they're in the same folder.

10. Install Java 11+.

11. To start the server, double-click the JAR file.

    If that doesn't work, open a terminal or command prompt in the same folder as the server JAR, and run this command: `java -jar TimeToEatServer-4.0-all.jar`

### Setting up the client
12. Copy `config.toml` and `alert.wav` to the client computer.

    If you want to use the Java client, also copy `TimeToEatJavaClient-4.0-all.jar`.

    If you want to use the Python client, copy `client.py`.

    `TimeToEatJavaClient-4.0-all.jar` is in the Compiled folder, and `client.py` is in the TimeToEatPythonClient folder.

13. If using the Java client, install Java 11+.

    If using the Python client, install Python 3+, and run the following commands in a terminal or command prompt: `pip3 install pyqt5 toml playsound`

14. In `config.toml` on the `id` line, replace `example` with the ID you chose for the computer. This ID must be in the `recipients` array in the server's copy of `config.toml`, or it won't work.

15. If you're using the Java client: Try double-clicking the JAR. If that doesn't work, run this command in the same folder as the JAR: `java -jar TimeToEatJavaClient-4.0-all.jar`

    If you're using the Python client, run this command in the same folder as `client.py`: `python3 client.py`

    Note that neither version of the program will show a message on startup; to test if it's working, you need to send a message.

### Using the server

16. To send the message, press the space bar in the server window. You can make a bigger space button using something like a [Makey Makey](https://makeymakey.com/).

### Mobile devices
While there is no iOS/Android version of the client yet, you can use [IFTTT](ifttt.com) to send the message to mobile devices.

1. Sign up or log in to IFTTT.

2. Click Create.

3. Click on "If This" and select Email.

4. Click "Send IFTTT any email".

5. Click on "Then That" and select Notifications.

6. Click "Send a notification from the IFTTT app".

7. In the Message field, enter your message. Click on Create action.

8. In `config.toml`, add `trigger@applet.ifttt.com` to the `recipients` array.

9. Install the IFTTT app on your phone or tablet.

10. Log in.

11. Enjoy!
